# Dekla Token

## Requirements
- NodeJS LTS ~8.9.4
- Truffle
- Ganache (https://truffleframework.com/ganache) - set up network with 20 users 

## Development
- Install
```npm install```

- Compile contract `truffle compile`

- Test contract `truffle test`

- Deploy contract `truffle migrate`
