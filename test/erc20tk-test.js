const ERC20Token = artifacts.require("DeklaToken");
const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

contract('DeklaToken Test', async(accounts) => {
  beforeEach(async function() {
    this.token = await ERC20Token.deployed(accounts[0], accounts[1], accounts[2], accounts[1],
      accounts[2], accounts[3], accounts[4],
      accounts[5], accounts[6], accounts[7], accounts[8], accounts[9], );
    this.owner = accounts[0];
  });

  describe('total supply', function() {
    it('returns the total amount of tokens', async function() {
      const totalSupply = await this.token.totalSupply();
      assert.equal(totalSupply.valueOf(), 1000000000);
    });

    it('returns token name, symbol and decimal', async function() {
        const tokenName = await this.token.name();
        assert.equal(tokenName, 'Dekla Token');
        const tokenSymbol = await this.token.symbol();
        assert.equal(tokenSymbol, 'DKL');
        const tokenDecimals = await this.token.decimals();
        assert.equal(tokenDecimals.valueOf(), 0);
    })

    it('accounts[1] transfer 200DKL to accounts[11]', async function() {
        await this.token.transfer(accounts[11], 200, {from: accounts[1]});
        const balance = await this.token.balanceOf.call(accounts[11]);
        assert.equal(balance.valueOf(), 200);
    });

    it(' account [1] approve account acount[12] 200DKL', async function() {
        await this.token.approve(accounts[12], 200,  {from: accounts[1]});
        const allowance = await this.token.allowance(accounts[1], accounts[12]);
        assert.equal(allowance.valueOf(), 200); 
    });

    it('accounts[12] transfer accounts[1] allowance to accounts[13]', async function() {
        await this.token.transferFrom(accounts[1], accounts[13], 200, {from: accounts[12]});
        const balance = await this.token.balanceOf.call(accounts[13]);
        assert.equal(balance.valueOf(), 200);
    })

    
  });
})