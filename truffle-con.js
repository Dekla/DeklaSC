module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      from: "0x107726dD92e58e54E90b62B999ADb82775F63d48", // default address to use for any transaction Truffle makes during migrations
      network_id: "*", // match any network
      gas: 7721975 // Gas limit used for deploys
    },
    rinkeby: {
      host: "localhost", // Connect to geth on the specified
      port: 7545,
      from: "0x107726dD92e58e54E90b62B999ADb82775F63d48", // default address to use for any transaction Truffle makes during migrations
      network_id: 4,
      gas: 7721975 // Gas limit used for deploys
    },
    solc: { optimizer: { enabled: true, runs: 200 } },

    // live: {
    //   host: "178.25.19.88", // Random IP for example purposes (do not use)
    //   port: 80,
    //   network_id: 1,        // Ethereum public network
    //   // optional config values:
    //   // gas
    //   // gasPrice
    //   // from - default address to use for any transaction Truffle makes during migrations
    //   // provider - web3 provider instance Truffle should use to talk to the Ethereum network.
    //   //          - function that returns a web3 provider instance (see below.)
    //   //          - if specified, host and port are ignored.
    // }
  }

};
